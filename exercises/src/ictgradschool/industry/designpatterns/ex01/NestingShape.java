package ictgradschool.industry.designpatterns.ex01;

import jdk.nashorn.internal.runtime.SharedPropertyMap;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {

    List<Shape> shapes = new ArrayList<>();

    private NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for(Shape shape: shapes){
            shape.move(this.fWidth, this.fHeight); //within the parent frame
        }
    }


    @Override
    //change the position of drasing shapes that is relative to NestingShape not to Origin AnimaitionViewer
    public void paint(Painter painter ){
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(this.fX,this.fY);
        for(Shape shape: shapes){
            shape.paint(painter);
        }
        //shold back to origin position otherwise the normal shapes of Animaition viewer that is drawn
        // after this translate() method would be out of Normal position
        painter.translate(-fX,-fY);
    }

    public void add(Shape child ) throws IllegalArgumentException{
     if(shapes.contains(child)){
         throw new IllegalArgumentException();
     }
     if(child.fWidth>this.fWidth||child.fHeight>this.fHeight){
         throw new IllegalArgumentException();
     }
     shapes.add(child);
     child._parent=this;
    }

    public void remove(Shape child ){
    shapes.remove(child);
    child._parent=null;
    }

    public Shape shapeAt( int index ) throws IndexOutOfBoundsException{
        return shapes.get(index);

    }

    public int shapeCount(){
        return shapes.size();
    }

    public int indexOf(Shape child ){
        shapes.indexOf(child);

        return indexOf(child);

    }

    public boolean contains(Shape child ){
        shapes.contains(child);
        return true;
    }


}
