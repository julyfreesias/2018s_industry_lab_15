package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

public class TreeModelAdapter implements TreeModel {

    private NestingShape _adaptee;

    public TreeModelAdapter(NestingShape root) {
         _adaptee = root;
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Shape result = null;

        if (parent instanceof NestingShape) {
            NestingShape shapelist = (NestingShape) parent;
            result = shapelist.shapeAt(index);
        }
        return result;

    }

    @Override
    public int getChildCount(Object parent) {
        return ((NestingShape)parent).shapeCount();
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
// dw
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {

        return ((NestingShape)parent).indexOf((Shape) child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
// dw
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
// dw
    }
}
